variable "templateImagePath" {
  type = string
  description = "Path to template image on filesystem."
}

variable "vmDataPoolName" {
  type = string
  default = "vmData"
  description = "VM data pool name."
}

variable "vmDataPoolDir" {
  type = string
  default = "/var/lib/libvirt/vmData"
  description = "Path to VM data pool."
}

variable "networkName" {
  type = string
  default = "default"
  description = "VM network name."
}

variable "rootPass" {
  description = "Password for root user."
  type = string
}

variable "pubKey" {
  type = string
  description = "SSH public key for root user."
}

variable "vmConfiguration" {
   type = object({
     ram = string,
     vcpu = number
   })
}

variable "vmNetwork" {
  type = string
  default = "192.168.122.0/24"
}

variable "netMask" {
  type = string
  default = "255.255.255.0"
}

variable "netGateway" {
  type = string
  default = "192.168.122.1"
}

variable "DNS1" {
  type = string
  default = "8.8.8.8"
}

variable "DNS2" {
  type = string
  default = "8.8.4.4"
}

variable "startNetAddress" {
  type = number
  default = 10
}

variable "vmNamePattern" {
  type = string
  default = "vm"
}

variable "vmNumber" {
  type = number
  default = 1
}



