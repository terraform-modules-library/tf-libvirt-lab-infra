provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_pool" "vmData" {
  name = var.vmDataPoolName
  type = "dir"
  path = var.vmDataPoolDir
}

resource "libvirt_volume" "vmDataVolume" {
  count = var.vmNumber
  name   = "${var.vmNamePattern}-${count.index + 1}"
  pool   = libvirt_pool.vmData.name
  source = var.templateImagePath
  format = "qcow2"
}

data "template_file" "user_data" {
  count = var.vmNumber
  template = file("${path.module}/cloud_init.yaml")
  vars =  {
    rootPass = var.rootPass
    pubKey = var.pubKey
    addr = cidrhost(var.vmNetwork, var.startNetAddress + count.index)
    mask = cidrnetmask(var.vmNetwork)
    gateway = var.netGateway
    dns1 = var.DNS1
    dns2 = var.DNS2
    hostname = "${var.vmNamePattern}-${count.index + 1}"
  }
}

resource "libvirt_cloudinit_disk" "commoninit" {
  count = var.vmNumber
  name           = "${var.vmNamePattern}-${count.index + 1}-commoninit.iso"
  user_data      = data.template_file.user_data[count.index].rendered
  pool           = libvirt_pool.vmData.name
}

resource "libvirt_domain" "labVMNode" {
  count = var.vmNumber
  name   = "${var.vmNamePattern}-${count.index + 1}"
  memory = var.vmConfiguration.ram
  vcpu   = var.vmConfiguration.vcpu
  cloudinit = libvirt_cloudinit_disk.commoninit[count.index].id
  network_interface {
    network_name = var.networkName
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.vmDataVolume[count.index].id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

